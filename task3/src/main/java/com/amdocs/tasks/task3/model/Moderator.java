package com.amdocs.tasks.task3.model;

/**
 * Class describing user with role Moderator. Can only delete comments
 */
public class Moderator extends User {
    public Moderator(String name) {
        super(name);
    }

    @Override
    //Moderators cannot create new comments
    public boolean canCreate(Comment comment) {
        return false;
    }

    @Override
    //Moderators cannot edit comments
    public boolean canEdit(Comment comment) {
        return false;
    }

    @Override
    //Moderators have the ability to delete any comment (to remove trolls)
    public boolean canDelete(Comment comment) {
        return true;
    }
}
