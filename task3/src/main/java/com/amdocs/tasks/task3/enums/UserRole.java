package com.amdocs.tasks.task3.enums;

import java.util.Arrays;

/**
 * Enum to store possible values of user role types
 */
public enum UserRole {
    USER("USER"),
    MODERATOR("MODERATOR"),
    ADMIN("ADMIN");

    private String userRole;

    UserRole(String userRole) {
        this.userRole = userRole;
    }

    public String getUserRole() {
        return userRole;
    }

    public static UserRole getUserRoleByStringIfExist(String role) {
        return Arrays.stream(UserRole.values()).filter(e -> e.getUserRole().
                equals(role)).findAny().orElse(null);
    }
}
