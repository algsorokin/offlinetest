package com.amdocs.tasks.task3.model;

import java.time.LocalDateTime;

/**
 * Class describing comment. Comment can also be a reply
 */
public class Comment {
    private User author;
    private String message;
    private LocalDateTime timestamp;
    private Comment repliedTo;


    public Comment(User author, String message, Comment repliedTo) {
        timestamp = LocalDateTime.now();
        this.author = author;
        this.message = message;
        this.repliedTo = repliedTo;
    }

    public Comment(Comment comment, Comment repliedTo) {
        timestamp = LocalDateTime.now();
        this.author = comment.getAuthor();
        this.message = comment.getMessage();
        this.repliedTo = repliedTo;
    }

    public Comment(User author, String message) {
        timestamp = LocalDateTime.now();
        this.author = author;
        this.message = message;
    }

    User getAuthor() {
        return author;
    }

    public String getMessage() {
        return message;
    }

    public Comment getRepliedTo() {
        return repliedTo;
    }

    /**
     * "$message by ${author->name}"
     * "$message by ${author->name} (replied to ${repliedTo->author->name})"
     */
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("\"").append(message).append("\" by ").append(author.getName()).
                append(repliedTo == null ? "" : "replied to " + repliedTo.getRepliedTo().getAuthor().getName());
        return sb.toString();
    }
}
