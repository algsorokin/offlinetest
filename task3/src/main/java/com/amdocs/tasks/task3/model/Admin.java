package com.amdocs.tasks.task3.model;

/**
 * Class describing user with role Admin. Can perform all actions: create, update, delete comments
 */
public class Admin extends User {
    public Admin(String name) {
        super(name);
    }

    @Override
    //Admins can create new comments
    public boolean canCreate(Comment comment) {
        return true;
    }

    @Override
    //Admins can create edit any comment
    public boolean canEdit(Comment comment) {
        return true;
    }

    @Override
    //Admins can delete any comment
    public boolean canDelete(Comment comment) {
        return true;
    }
}
