package com.amdocs.tasks.task3.service;

import com.amdocs.tasks.task3.enums.UserRole;
import com.amdocs.tasks.task3.model.User;

/**
 * This interface represents a mock user service.
 * It is assumed all validations are on the view or controller layer
 */
public interface MockUserService {

    /**
     * @param role text name of the role {@link UserRole}
     * @param name user name
     * @return created User object
     */
    User createUser(String role, String name);

    /**
     * @param user User whose last login time we want to receive
     * @return
     */
    String getLastLoggedInTimestamp(User user);
}
