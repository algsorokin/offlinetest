package com.amdocs.tasks.task3.service;

import com.amdocs.tasks.task3.enums.UserRole;
import com.amdocs.tasks.task3.model.Admin;
import com.amdocs.tasks.task3.model.Moderator;
import com.amdocs.tasks.task3.model.User;

/**
 * Implementation of the interface {@link MockUserService}
 */
public class MockUserServiceImpl implements MockUserService {
    @Override

    public User createUser(String role, String name) {
        UserRole userRole = UserRole.getUserRoleByStringIfExist(role);
        if (userRole == null) {
            throw new IllegalArgumentException("User role is not exist.");
        }

        User user = null;
        switch (userRole) {
            case USER:
                user = new User(name);
                break;
            case ADMIN:
                user = new Admin(name);
                break;
            case MODERATOR:
                user = new Moderator(name);
                break;
        }

        //Here we should call the method of repository-bean to save the user in the database
        return user;
    }

    @Override
    public String getLastLoggedInTimestamp(User user) {
        if (user.getLastLoggedInAt() == null) return "User has never logged in.";
        else return user.getLastLoggedInAt().toString();
    }


}
