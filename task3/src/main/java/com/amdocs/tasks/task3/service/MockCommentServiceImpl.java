package com.amdocs.tasks.task3.service;

import com.amdocs.tasks.task3.model.Comment;
import com.amdocs.tasks.task3.model.User;

/**
 * Implementation of the interface {@link MockCommentService}
 */
public class MockCommentServiceImpl implements MockCommentService {
    @Override
    public void createComment(User user, Comment comment) {
        if (user.canCreate(comment)) {
            //Here we should call the method of repository-bean to save the comment in the database
        }
    }

    @Override
    public void updateComment(User user, Comment comment) {
        if (user.canEdit(comment)) {
            //Here we should call the method of repository-bean to modify the comment in the database
        }
    }

    @Override
    public void deleteComment(User user, Comment comment) {
        if (user.canDelete(comment)) {
            //Here we should call the method of repository-bean to modify the comment in the database
        }
    }

    @Override
    public void replyToComment(User user, Comment originalComment, Comment replyComment) {
        Comment newCommentWithReply = new Comment(replyComment, originalComment);
        createComment(user, newCommentWithReply);
    }

    @Override
    public Comment getOriginalComment(Comment comment) {
        return comment.getRepliedTo();
    }
}
