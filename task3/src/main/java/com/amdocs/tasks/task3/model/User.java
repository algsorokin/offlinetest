package com.amdocs.tasks.task3.model;

import java.time.LocalDateTime;

/**
 * Class describing a user with normal user role. Can create comments, can update his own comments,
 * can delete replies to his own comments
 */
public class User {
    private String name;
    private boolean isLoggedIn = false;
    private LocalDateTime lastLoggedInAt = null;

    public User(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    //We track when they last logged in
    public LocalDateTime getLastLoggedInAt() {
        return lastLoggedInAt;
    }

    //Users can log in
    public void logIn() {
        isLoggedIn = true;
        lastLoggedInAt = LocalDateTime.now();
    }

    //Users can log out
    public void logOut() {
        isLoggedIn = false;
    }

    //user can only create new comments
    public boolean canCreate(Comment comment) {
        return true;
    }

    //user can edit his own comments
    public boolean canEdit(Comment comment) {
        return comment.getAuthor().getName().equals(name);
    }

    //user can delete replies to his own comments
    public boolean canDelete(Comment comment) {
        return comment.getRepliedTo().getAuthor().getName().equals(name);
    }

}
