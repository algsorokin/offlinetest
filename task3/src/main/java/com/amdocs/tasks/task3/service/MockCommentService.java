package com.amdocs.tasks.task3.service;

import com.amdocs.tasks.task3.model.Comment;
import com.amdocs.tasks.task3.model.User;

/**
 * This interface represents a mock comment service.
 * It is assumed all validations are on the view or controller layer
 */
public interface MockCommentService {

    /**
     * @param user    which create a comment
     * @param comment - new comment to save in the base
     */
    void createComment(User user, Comment comment);

    /**
     * @param user    which update a comment
     * @param comment comment to update in the base
     */
    void updateComment(User user, Comment comment);

    /**
     * @param user    which delete a comment
     * @param comment comment to delete in the base
     */
    void deleteComment(User user, Comment comment);

    /**
     * @param user            which create a reply for comment
     * @param originalComment - comment to which the user replies
     * @param replyComment    new reply to comment
     */
    void replyToComment(User user, Comment originalComment, Comment replyComment);

    /**
     * @param comment reply to comment
     * @return original comment to which was replied
     */
    Comment getOriginalComment(Comment comment);
}
