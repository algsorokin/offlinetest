package com.amdocs.tasks.task2.entity;

import java.time.LocalDateTime;

/**
 * Class represents the article objects which will store in the database
 */
public class Article {
    private Integer id;
    private String header;
    private String body;

    private LocalDateTime timeStamp;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getHeader() {
        return header;
    }

    public void setHeader(String header) {
        this.header = header;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public LocalDateTime getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(LocalDateTime timeStamp) {
        this.timeStamp = timeStamp;
    }

    @Override
    public String toString() {
        return "Article{" +
                "id=" + id +
                ", header='" + header + '\'' +
                ", body='" + body + '\'' +
                ", timeStamp=" + timeStamp +
                '}';
    }
}
