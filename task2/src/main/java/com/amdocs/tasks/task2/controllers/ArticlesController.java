package com.amdocs.tasks.task2.controllers;

import com.amdocs.tasks.task2.entity.Article;
import com.amdocs.tasks.task2.service.ArticleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/article")
public class ArticlesController {

    @Autowired
    ArticleService articlesService;

    @PostMapping("/create")
    public ResponseEntity createArticle(@RequestBody Article article) {
        String errors = articlesService.createArticle(article);
        if (!errors.isEmpty()) {
            return ResponseEntity.badRequest().body(errors + article); //it can be exception here instead of ResponseEntity
        }
        return ResponseEntity.ok(article);
    }

    @PostMapping("/update")
    public ResponseEntity updateArticle(@RequestBody Article article) {
        String errors = articlesService.modifyArticle(article);
        return ResponseEntity.ok(errors + article); //it can be exception here in case of errors instead of ResponseEntity
    }

    @GetMapping("/retrieveById")
    public ResponseEntity retrieveArticleById(@RequestParam("id") Integer id) {
        Article article = articlesService.retrieveArticleById(id);
        if (article == null) {
            return ResponseEntity.ok("Article not found, id = " + id); //it can be exception here instead of ResponseEntity
        }
        return ResponseEntity.ok(article);
    }

    @GetMapping("/retrieveAll")
    public ResponseEntity retrieveAllArticles() {
        List<Article> allArticles = articlesService.retrieveAllArticles();
        if (CollectionUtils.isEmpty(allArticles)) {
            return ResponseEntity.ok("There are no articles in the database");
        }
        return ResponseEntity.ok(allArticles);
    }

    @GetMapping("/retrieveAllByDate")
    public ResponseEntity retrieveAllArticlesForSpecificDate(
            @RequestParam("date") @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date) {
        List<Article> allArticles = articlesService.retrieveAllArticlesForSpecificDate(date);
        if (CollectionUtils.isEmpty(allArticles)) {
            return ResponseEntity.ok("There are no articles in the database for the date " + date);
        }
        return ResponseEntity.ok(allArticles);
    }

    @DeleteMapping("/delete")
    public ResponseEntity<String> deleteArticle(@RequestParam("id") Integer id) {
        String result = articlesService.deleteArticleById(id);
        return ResponseEntity.ok(result);
    }
}
