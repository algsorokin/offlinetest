package com.amdocs.tasks.task2.service;

import com.amdocs.tasks.task2.dao.ArticleSimpleRepository;
import com.amdocs.tasks.task2.entity.Article;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Implementation of the interface {@link ArticleService}
 */
@Service
public class ArticleServiceImpl implements ArticleService {
    @Autowired
    ArticleSimpleRepository repository;

    //TODO I wanted to do validation with javax.validation.constraints but it is external library =(
    public String validateArticle(Article article, boolean isNewArticle) {
        StringBuilder sb = new StringBuilder();

        if (!isNewArticle && article.getId() == null) {
            sb.append("The id should be filled\n");
        }
        if (article.getHeader() == null) {
            sb.append("The header should be filled.\n");
        } else if (article.getHeader().length() > 64) {
            sb.append("The header must be less than 64 symbols length.\n");
        }
        if (article.getBody() == null) {
            sb.append("The body should be filled.\n");
        }
        return sb.toString();
    }

    @Override
    public String createArticle(Article article) {
        article.setTimeStamp(LocalDateTime.now());
        String validationResult = validateArticle(article, true);
        if (validationResult.isEmpty()) {
            repository.saveArticle(article);
        }
        return validationResult;
    }

    @Override
    public String modifyArticle(Article article) {
        article.setTimeStamp(LocalDateTime.now());
        String errors = validateArticle(article, false);
        if (errors.isEmpty()) {
            if (repository.findArticleById(article.getId()) != null) {
                repository.saveArticle(article);
            } else {
                errors = String.format("Article with id %d is not found.\n", article.getId());
            }
        }
        return errors;
    }

    @Override
    public Article retrieveArticleById(Integer id) {
        return repository.findArticleById(id);
    }

    @Override
    public List<Article> retrieveAllArticles() {
        return repository.findAllDsc();
    }

    @Override
    public List<Article> retrieveAllArticlesForSpecificDate(LocalDate date) {
        return repository.findAllForSpecificDateDsc(date);
    }

    @Override
    public String deleteArticleById(Integer id) {
        String result;
        Article article = retrieveArticleById(id);
        if (article == null) {
            result = "Fail. Article not found, id = " + id;
        } else if (article.getTimeStamp().isAfter(LocalDateTime.now().minusYears(1))) {
            result = "Not allowed. The article is pretty fresh to delete. " + article;
        } else {
            repository.deleteArticleById(id);
            result = article + " - has deleted successfully";
        }
        return result;
    }
}
