package com.amdocs.tasks.task2.dao;


import com.amdocs.tasks.task2.entity.Article;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

@Repository
/**
 * Implementation of the interface {@link ArticleSimpleRepository}
 */
public class ArticleSimpleRepositoryImpl implements ArticleSimpleRepository {
    private static int idCounter = 0;
    private static final HashMap<Integer, Article> simpleRepoMap = new HashMap<>();

    @Override
    public void saveArticle(Article article) {
        if (article.getId() == null) article.setId(++idCounter); //Just to avoid creating an additional method for
        simpleRepoMap.put(article.getId(), article);             //the modifying article
    }

    @Override
    public Article findArticleById(int id) {
        return simpleRepoMap.get(id);
    }

    @Override
    public List<Article> findAllForSpecificDateDsc(LocalDate date) {
        List<Article> list = simpleRepoMap.values().stream().
                filter(e -> e.getTimeStamp().toLocalDate().equals(date)).
                sorted((o1, o2) -> o2.getTimeStamp().compareTo(o1.getTimeStamp())).
                collect(Collectors.toList());
        return list;
    }

    @Override
    public List<Article> findAllDsc() {
        List<Article> result = new ArrayList<>(simpleRepoMap.values());
        result.sort((o1, o2) -> o2.getTimeStamp().compareTo(o1.getTimeStamp()));
        return result;
    }

    @Override
    public void deleteArticleById(int id) {
        simpleRepoMap.remove(id);
    }

}
