package com.amdocs.tasks.task2.service;


import com.amdocs.tasks.task2.entity.Article;

import java.time.LocalDate;
import java.util.List;

/**
 * This interface represents service for for working with articles
 */
public interface ArticleService {
    /**
     * @param article      for validation
     * @param isNewArticle flag to skip ID verification in new articles
     * @return all errors if exists
     */
    String validateArticle(Article article, boolean isNewArticle);

    /**
     * @param article to create
     * @return all errors if exists
     */
    String createArticle(Article article);

    /**
     * @param article to modify
     * @return all errors if exists
     */
    String modifyArticle(Article article);

    /**
     * @param id of the article to retrieve
     * @return Article if it was found or null, if not
     */
    Article retrieveArticleById(Integer id);

    /**
     * @return All articles if exists or null, if not
     */
    List<Article> retrieveAllArticles();

    /**
     * @param date of creation of articles to be retrieved
     * @return All articles for specific date if exists or null, if not
     */
    List<Article> retrieveAllArticlesForSpecificDate(LocalDate date);

    /**
     * @param id of the article to delete
     * @return result or error
     */
    String deleteArticleById(Integer id);
}
