package com.amdocs.tasks.task2.dao;


import com.amdocs.tasks.task2.entity.Article;

import java.time.LocalDate;
import java.util.List;

/**
 * This interface represents simple data storage for the articles. Organised with simple in-memory structure
 * (it gets flushed if the application restarted
 */
public interface ArticleSimpleRepository {

    /**
     * @param article to save in database
     */
    void saveArticle(Article article);

    /**
     * @param id of the article to find in the database
     * @return
     */
    Article findArticleById(int id);

    /**
     * @param date of creation of articles to be obtained from the database
     * @return all articles for provided date
     */
    List<Article> findAllForSpecificDateDsc(LocalDate date);

    /**
     * @return all articles in the database
     */
    List<Article> findAllDsc();

    /**
     * @param id of the article to delete in the database
     */
    void deleteArticleById(int id);
}
