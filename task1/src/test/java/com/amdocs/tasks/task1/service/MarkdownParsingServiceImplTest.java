package com.amdocs.tasks.task1.service;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Tests for {@link MarkdownParsingService}
 */
public class MarkdownParsingServiceImplTest {
    private static final String VALID_H1_RESULT = "<h1>This is the H1 header</h1>";
    private static final String VALID_H2_RESULT = "<h2>This is the H2 header with 3 spaces at the beginning and 3 at " +
            "the end</h2>";
    private static final String VALID_H4_RESULT = "<h4>This is the H4 header with #### - hashes in the content</h4>";
    private static final String VALID_H5_RESULT = "<h5>This is H5 header with !<random$% symbols</h5>";

    private MarkdownParsingService markdownParsingService = new MarkdownParsingServiceImpl();

    @Test
    public void header1SuccessParsingTest() {
        String markdownStr = "# This is the H1 header";
        assertEquals(VALID_H1_RESULT, markdownParsingService.parseHeader(markdownStr));
    }

    @Test
    public void header2WithStartEndSpacesSuccessParsingTest() {
        String markdownStr = "   ## This is the H2 header with 3 spaces at the beginning and 3 at the end   ";
        assertEquals(VALID_H2_RESULT, markdownParsingService.parseHeader(markdownStr));
    }

    @Test
    public void header3FailParsingNoSpaceAfterHashesTest() {
        String markdownStr = "##This is the H3 header, but there is no space after hashes";
        assertEquals(markdownStr, markdownParsingService.parseHeader(markdownStr));
    }

    @Test
    public void header4SuccessParsingWithHashesInTheContentTest() {
        String markdownStr = "#### This is the H4 header with #### - hashes in the content";
        assertEquals(VALID_H4_RESULT, markdownParsingService.parseHeader(markdownStr));
    }

    @Test
    public void header5SuccessParsingWithRandomSymbolsTest() {
        String markdownStr = "##### This is H5 header with !<random$% symbols";
        assertEquals(VALID_H5_RESULT, markdownParsingService.parseHeader(markdownStr));
    }

    @Test
    public void header16IsNotExistFailParsingTest() {
        String markdownStr = "################ This is H16 header. It's not exists";
        assertEquals(markdownStr, markdownParsingService.parseHeader(markdownStr));
    }


}