package com.amdocs.tasks.task1.service;

/**
 * Interface for parsing markdown syntax https://en.wikipedia.org/wiki/Markdown
 */
public interface MarkdownParsingService {

    /**
     * Simple markdown parser function that will take in a single line of markdown and be translated into the
     * appropriate HTML
     *
     * @param markdownString -  single line of markdown header
     * @return the header in HTML syntax in case of success, unchanged markdownString in case of fail
     */
    String parseHeader(String markdownString);
}
