package com.amdocs.tasks.task1.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Implementation of the interface {@link MarkdownParsingService} for parsing markdown syntax
 * https://en.wikipedia.org/wiki/Markdown
 */
public class MarkdownParsingServiceImpl implements MarkdownParsingService {
    private static final Logger logger = LoggerFactory.getLogger(MarkdownParsingServiceImpl.class);

    @Override
    public String parseHeader(String markdownString) {
        logger.info("Received markdown string: {}", markdownString);

        Pattern regexFormat = Pattern.compile("^\\s*(#{1,6}?)\\s+(.+?)\\s*$");
        Matcher matcher = regexFormat.matcher(markdownString);
        if (matcher.matches()) {
            int headerLevel = matcher.group(1).length();
            StringBuilder sb = new StringBuilder("<h").append(headerLevel).append(">").
                    append(matcher.group(2)).append("</h").append(headerLevel).append(">");
            String result = sb.toString();

            logger.info("Success. Result HTML-tag: {}", result);
            return result;
        }
        logger.info("Fail. Unchanged markdown string will return: {}", markdownString);
        return markdownString;
    }
}
